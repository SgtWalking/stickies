var Colors = {
    pink: '#ff7eb9',
    blue: '#7afcff',
    yellow: '#feff9c'
};
var random = function (max) { return (Math.random() * max) | 0; };
var Sticky = /** @class */ (function () {
    function Sticky(head, body, bgColor) {
        this.head = head;
        this.body = body;
        this.bgColor = Colors[bgColor];
        this.isArray = Array.isArray(this.body);
    }
    Sticky.prototype.bodyToString = function () {
        var out = '';
        if (this.isArray)
            for (var i = 0; i < this.body.length; i++)
                out += i < this.body.length - 1 ? this.body[i] + ' - ' : this.body[i];
        else
            out = this.body;
        return out;
    };
    Sticky.prototype.bodyToList = function () {
        var out = '';
        if (this.isArray)
            for (var i = 0; i < this.body.length; i++)
                out += "<li>" + this.body[i] + "</li>";
        else
            out = "<li>" + this.body + "</li>";
        return "<ul>" + out + "</ul>";
    };
    Sticky.prototype.listString = function (body) {
        return "\n      <div class=\"sticky\" style=\"background:" + this.bgColor + "\">\n        <div class=\"head\">\n          <button class=\"btn btn--close\">Close</button>\n          <p>" + this.head + "</p>\n        </div>\n        <div class=\"body\">" + body + "</div>\n      </div>";
    };
    Sticky.prototype.display = function () {
        var parent = document.body;
        this.isArray
            ? parent.insertAdjacentHTML('beforeend', this.listString(this.bodyToList()))
            : parent.insertAdjacentHTML('beforeend', this.listString(this.body));
    };
    return Sticky;
}());
//# sourceMappingURL=sticky.js.map
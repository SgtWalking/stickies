const Colors: { pink: string; blue: string; yellow: string } = {
  pink: '#ff7eb9',
  blue: '#7afcff',
  yellow: '#feff9c'
}

const random = (max: number): number => (Math.random() * max) | 0;

class Sticky {
  head: string;
  body: string | string[];
  bgColor: string;
  private isArray: boolean;

  constructor(head: string, body: string | string[], bgColor: string) {
    this.head = head;
    this.body = body;
    this.bgColor = Colors[bgColor];
    this.isArray = Array.isArray(this.body);
  }

  private bodyToString(): string {
    let out: string = '';
    if (this.isArray)
      for (let i: number = 0; i < this.body.length; i++)
        out += i < this.body.length - 1 ? this.body[i] + ' - ' : this.body[i];
    else out = this.body as string;
    return out;
  }

  private bodyToList(): string {
    let out: string = '';
    if (this.isArray)
      for (let i: number = 0; i < this.body.length; i++)
        out += `<li>${this.body[i]}</li>`;
    else out = `<li>${this.body}</li>`;
    return `<ul>${out}</ul>`;
  }

  private listString(body: string): string {
    return `
      <div class="sticky" style="background:${this.bgColor}">
        <div class="head">
          <button class="btn btn--close">Close</button>
          <p>${this.head}</p>
        </div>
        <div class="body">${body}</div>
      </div>`;
  }

  display() {
    let parent: HTMLElement = document.body;
    this.isArray
      ? parent.insertAdjacentHTML(
          'beforeend',
          this.listString(this.bodyToList())
        )
      : parent.insertAdjacentHTML(
          'beforeend',
          this.listString(this.body as string)
        );
  }
}

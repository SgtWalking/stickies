let modalActive = false;
const closeModal = () => {
  document.body.removeChild(document.querySelector('.modal'));
};

const modal = () => {
  if (!modalActive) {
    document.body.insertAdjacentHTML(
      'beforeend',
      `<div class="modal">
      <div class="modalData">
        <p class="head">Add sticky</p>
        <button class="btn btn--modal--close body">Close</button>

        <p class="body">Header</p>
        <input class="body header" type="text" />
        <p class="body">Items</p>
        <input class="body items" type="text" placeholder="Item1, Item2, ..." />
        <p class="body">Color</p>

        <select class="color body">
          <option selected>yellow</option>
          <option>blue</option>
          <option>pink</option>
        </select>
        <button class="btn btn--modal body">Add sticky</button>
      </div>
    </div>`
    );

    document
      .querySelector('.btn--modal--close')
      .addEventListener('click', () => closeModal());

    document.querySelector('.btn--modal').addEventListener('click', () => {
      // Get data and make new sticky
      let header: HTMLInputElement = document.querySelector('.header');
      let items: HTMLInputElement = document.querySelector('.items');
      let itemsList: string[] = items.value.split(',');
      let color: HTMLSelectElement = document.querySelector('.color');

      let sticky: Sticky = new Sticky(
        header.value,
        itemsList,
        color.options[color.selectedIndex].value
      );
      closeModal();

      sticky.display();
      $('.sticky').draggable();

      // Add close sticky button functionality
      $('.btn--close').on('click', (e: Event) => {
        let target: HTMLButtonElement = e.target as HTMLButtonElement;
        let sticky: HTMLElement = target.parentElement.parentElement;
        sticky.parentNode.removeChild(sticky);
      });
    });
  } else {
    closeModal();
    modalActive = !modalActive;
  }
};

// Add sticky
$('.btn--add').on('click', () => modal());

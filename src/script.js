var modalActive = false;
var closeModal = function () {
    document.body.removeChild(document.querySelector('.modal'));
};
var modal = function () {
    if (!modalActive) {
        document.body.insertAdjacentHTML('beforeend', "<div class=\"modal\">\n      <div class=\"modalData\">\n        <p class=\"head\">Add sticky</p>\n        <button class=\"btn btn--modal--close body\">Close</button>\n\n        <p class=\"body\">Header</p>\n        <input class=\"body header\" type=\"text\" />\n        <p class=\"body\">Items</p>\n        <input class=\"body items\" type=\"text\" placeholder=\"Item1, Item2, ...\" />\n        <p class=\"body\">Color</p>\n\n        <select class=\"color body\">\n          <option selected>yellow</option>\n          <option>blue</option>\n          <option>pink</option>\n        </select>\n        <button class=\"btn btn--modal body\">Add sticky</button>\n      </div>\n    </div>");
        document
            .querySelector('.btn--modal--close')
            .addEventListener('click', function () { return closeModal(); });
        document.querySelector('.btn--modal').addEventListener('click', function () {
            // Get data and make new sticky
            var header = document.querySelector('.header');
            var items = document.querySelector('.items');
            var itemsList = items.value.split(',');
            var color = document.querySelector('.color');
            var sticky = new Sticky(header.value, itemsList, color.options[color.selectedIndex].value);
            closeModal();
            sticky.display();
            $('.sticky').draggable();
            // Add close sticky button functionality
            $('.btn--close').on('click', function (e) {
                var target = e.target;
                var sticky = target.parentElement.parentElement;
                sticky.parentNode.removeChild(sticky);
            });
        });
    }
    else {
        closeModal();
        modalActive = !modalActive;
    }
};
// Add sticky
$('.btn--add').on('click', function () { return modal(); });
//# sourceMappingURL=script.js.map